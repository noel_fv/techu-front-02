

## Install the Polymer-CLI

First, make sure you have the [Polymer CLI](https://www.npmjs.com/package/polymer-cli) installed. Then run `polymer serve` to serve your application locally.


## Build Your Application

```
$ bower i
$ npm i

## Viewing Your Application

```
$ polymer serve
$ polymer serve --open
$ polymer serve --open --port 8081
```

## Building Your Application

```
$ polymer build
$ polymer build --name v3
```

This will create builds of your application in the `build/` directory, optimized to be served in production. You can then serve the built versions by giving `polymer serve` a folder to serve from:

```
$ polymer serve build/default
```

## Running Tests

```
$ polymer test
```

Your application is already set up to be tested via [web-component-tester](https://github.com/Polymer/web-component-tester). Run `polymer test` to run your application's test suite locally.



## Para la integracion con northwind
Es necesario colocar la url donde se encuentra
desplegado el servicio, y en el JSON.parse  es necesario colocar el .value
```
obtenerClientes(){
  var request = new XMLHttpRequest();
  request.open("GET","https://services.odata.org/V4/Northwind/Northwind.svc/Customers",false);
  request.setRequestHeader("Accept","application/json");
  request.send();
  this.clientes = JSON.parse(request.responseText).value;
  console.log(this.clientes);
}
```

## Para la integracion con mlab(mongodb)
Es necesario colocar la url donde se encuentra
desplegado el servicio, y en el JSON.parse no es necesario colocar el .value

```
obtenerClientes(){
  var request = new XMLHttpRequest();
  request.open("GET","http://localhost:2019/movimientos",false);
  request.setRequestHeader("Accept","application/json");
  request.send();
  this.clientes = JSON.parse(request.responseText);
  console.log(this.clientes);
}
```
