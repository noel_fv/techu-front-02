//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

//para que funcione el npm start en polymer se tiene que agregar el siguiente valor
//tiene que apuntar al nombre del build con el que fue construido
app.use(express.static(__dirname+'/build/default'));

app.listen(port);

console.log('aplicacion polymer desde node: ' + port);

/* invocando con polymer
app.get("/",function(req,res){
   res.sendFile(path.join('index.html'));
})
*/

//invocando con node
app.get("/",function(req,res){
  res.sendFile(path.join('index.html',{root:'.'}));
})
